package postgres

import (
	"database/sql"
	"fmt"
	pb "genproto/user_service"
	"time"

	// "github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"bitbucket.org/alien_soft/delever_user_service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (cm *userRepo) Create(client *pb.Client) (*pb.Client, error) {
	tx, err := cm.db.Begin()

	if err != nil {
		return nil, err
	}
	insertNew :=
		`INSERT INTO
		users
		(
			id,
			access_token,
			name,
			phone,
			user_name,
			bio,
			photo
		)
		VALUES
		($1, $2, $3, $4,$5,$6,$7)`
	_, err = tx.Exec(
		insertNew,
		client.GetId(),
		client.GetAccessToken(),
		client.GetName(),
		client.GetPhone(),
		client.GetUserName(),
		client.GetBio(),
		client.GetPhoto(),
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetClient(client.GetId())
	if err != nil {
		return nil, err
	}
	fmt.Print(client.GetId())

	return c, nil

}

func (cm *userRepo) Update(client *pb.Client) (*pb.Client, error) {

	tx, err := cm.db.Begin()
	if err != nil {
		return nil, err
	}
	updateQuery :=
		`UPDATE users
		 SET
		    name=$1,
			phone=$2,
			is_active=$3,
			user_name=$4,
			bio=$5,
			photo=$6
		WHERE id=$7`

	_, err = tx.Exec(
		updateQuery,
		client.GetName(),
		client.GetPhone(),
		client.GetIsActive(),
		client.GetUserName(),
		client.GetBio(),
		client.GetPhoto(),
		client.GetId(),
	)
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetClient(client.GetId())
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cm *userRepo) Delete(id string) error {
	result, err := cm.db.Exec(`UPDATE users SET deleted_at=CURRENT_TIMESTAMP where id=$1 and deleted_at IS NULL`, id)
	if err != nil {
		return err
	}
	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (cm *userRepo) GetClient(id string) (*pb.Client, error) {
	var (
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		client     pb.Client
	)

	row := cm.db.QueryRow(`
		SELECT  id,
				name,
				access_token,
				phone,
				user_name,
				bio,
				photo, 
				is_active,
				created_at
		FROM users
		WHERE id=$1 and
		deleted_at IS NULL`, id,
	)

	err := row.Scan(
		&client.Id,
		&client.Name,
		&client.AccessToken,
		&client.Phone,
		&client.UserName,
		&client.Bio,
		&client.Photo,
		&client.IsActive,
		&createdAt,
	)
	if err != nil {
		return nil, err
	}
	client.CreatedAt = createdAt.Format(layoutDate)
	if err != nil {
		return nil, err
	}
	return &client, nil
}

func (cm *userRepo) GetClients(page, limit uint64) ([]*pb.Client, uint64, error) {
	var (
		count      uint64
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		clients    []*pb.Client
	)

	offset := (page - 1) * limit
	query := `
		SELECT  id,
				access_token,
				name,
				phone,
				user_name,
				bio,
				photo,
				is_active,
				created_at
		FROM users
		WHERE deleted_at IS NULL 
		ORDER BY created_at DESC 
		LIMIT $1 OFFSET $2`
	rows, err := cm.db.Queryx(query, limit, offset)

	if err != nil {
		return nil, 0, err
	}
	for rows.Next() {
		var c pb.Client
		err = rows.Scan(
			&c.Id,
			&c.AccessToken,
			&c.Name,
			&c.Phone,
			&c.UserName,
			&c.Bio,
			&c.Photo,
			&c.IsActive,
			&createdAt,
		)
		if err != nil {
			return nil, 0, err
		}
		c.CreatedAt = createdAt.Format(layoutDate)
		clients = append(clients, &c)
	}
	row := cm.db.QueryRow(`
		SELECT count(1) 
		FROM users
		WHERE deleted_at IS NULL`,
	)

	err = row.Scan(
		&count,
	)

	return clients, count, nil
}



func (cm *userRepo) SearchClientsByName(name string, limit uint64) ([]*pb.Client, error) {
	var clients []*pb.Client

	query := `
			SELECT  id,
					phone,
					name,
					user_name,
					bio,
					photo
			FROM users
			WHERE name LIKE '%' || $1 || '%' and 
			deleted_at IS NULL
			ORDER BY created_at DESC
			LIMIT $2 `
	rows, err := cm.db.Queryx(query, name, limit)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var c pb.Client
		err = rows.Scan(
			&c.Id,
			&c.Phone,
			&c.Name,
			&c.UserName,
			&c.Bio,
			&c.Photo,
		)

		if err != nil {
			return nil, err
		}
		clients = append(clients, &c)
	}
	return clients, nil
}
