package postgres

import (
	"database/sql"
	"fmt"
	pb "genproto/user_service"
	"time"

	// "github.com/google/uuid"

	"github.com/jmoiron/sqlx"

	"bitbucket.org/alien_soft/delever_user_service/storage/repo"
)

type groupRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewGroupRepo(db *sqlx.DB) repo.GroupStorageI {
	return &groupRepo{
		db: db,
	}
}

func (cm *groupRepo) Create(group *pb.Group) (*pb.Group, error) {
	tx, err := cm.db.Begin()

	if err != nil {
		return nil, err
	}
	insertNew :=
		`INSERT INTO
		groups
		(
			id,
			access_token,
			name,
			user_name,
			bio,
			photo,
			owner,
			users,
			admin
		)
		VALUES
		($1, $2, $3, $4,$5,$6,$7,'{`+group.GetOwner()+`}','{`+group.GetOwner()+`}')`
	_, err = tx.Exec(
		insertNew,
		group.GetId(),
		group.GetAccessToken(),
		group.GetName(),
		group.GetUserName(),
		group.GetBio(),
		group.GetPhoto(),
		group.GetOwner(),
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group.GetId())
	if err != nil {
		return nil, err
	}
	fmt.Print(group.GetId())

	return c, nil

}
func (cm *groupRepo) Update(group *pb.Group) (*pb.Group, error) {

	tx, err := cm.db.Begin()
	if err != nil {
		return nil, err
	}
	updateQuery :=
		`UPDATE groups
		 SET
		    name=$1,
			user_name=$3,
			bio=$4,
			photo=$5
		WHERE id=$2`

	_, err = tx.Exec(
		updateQuery,
		group.GetName(),
		group.GetId(),
		group.GetUserName(),
		group.GetBio(),
		group.GetPhoto(),
	)
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group.GetId())
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cm *groupRepo) Delete(id string) error {
	result, err := cm.db.Exec(`UPDATE groups SET deleted_at=CURRENT_TIMESTAMP where id=$1 and deleted_at IS NULL`, id)
	if err != nil {
		return err
	}
	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (cm *groupRepo) GetGroup(id string) (*pb.Group, error) {
	var (
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		group      pb.Group
	)

	row := cm.db.QueryRow(`
		SELECT  id,
				name,
				access_token,
				user_name,
				bio,
				photo, 
				owner,
				admin,
				users,
				created_at
		FROM groups
		WHERE id=$1 and
		deleted_at IS NULL`, id,
	)

	err := row.Scan(
		&group.Id,
		&group.Name,
		&group.AccessToken,
		&group.UserName,
		&group.Bio,
		&group.Photo,
		&group.Owner,
		&group.Admin,
		&group.Users,
		&createdAt,
	)
	if err != nil {
		return nil, err
	}
	group.CreatedAt = createdAt.Format(layoutDate)
	if err != nil {
		return nil, err
	}
	return &group, nil
}

func (cm *groupRepo) GetGroups(page, limit uint64) ([]*pb.Group, uint64, error) {
	var (
		count      uint64
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		groups     []*pb.Group
	)

	offset := (page - 1) * limit
	query := `
		SELECT  id,
				access_token,
				name,
				user_name,
				bio,
				photo,
				owner,
				users,
				admin,
				created_at
		FROM groups
		WHERE deleted_at IS NULL 
		ORDER BY created_at DESC 
		LIMIT $1 OFFSET $2`
	rows, err := cm.db.Queryx(query, limit, offset)

	if err != nil {
		return nil, 0, err
	}
	for rows.Next() {
		var c pb.Group
		err = rows.Scan(
			&c.Id,
			&c.AccessToken,
			&c.Name,
			&c.UserName,
			&c.Bio,
			&c.Photo,
			&c.Owner,
			&c.Users,
			&c.Admin,
			&createdAt,
		)
		if err != nil {
			return nil, 0, err
		}
		c.CreatedAt = createdAt.Format(layoutDate)
		groups = append(groups, &c)
	}
	row := cm.db.QueryRow(`
		SELECT count(1) 
		FROM groups
		WHERE deleted_at IS NULL`,
	)

	err = row.Scan(
		&count,
	)

	return groups, count, nil
}

func (cm *groupRepo) SearchGroupByName(name string, limit uint64) ([]*pb.Group, error) {
	var groups []*pb.Group

	query := `
			SELECT  id,
					name,
					user_name,
					bio,
					photo,
					owner
			FROM groups
			WHERE name LIKE '%' || $1 || '%' and 
			deleted_at IS NULL
			ORDER BY created_at DESC
			LIMIT $2 `
	rows, err := cm.db.Queryx(query, name, limit)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var c pb.Group
		err = rows.Scan(
			&c.Id,
			&c.Name,
			&c.UserName,
			&c.Bio,
			&c.Photo,
			&c.Owner,
		)

		if err != nil {
			return nil, err
		}
		groups = append(groups, &c)
	}
	return groups, nil
}
func (cm *groupRepo) AddMembers(user_id , group_id string) (*pb.Group, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update groups SET  users = array_append(users,$1) from users where users.id=$1   and groups.id=$2 and $1!=all(groups.users)

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		group_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group_id)
	if err != nil {
		return nil, err
	}

	return c, nil
}
func (cm *groupRepo) AddAdmins(user_id, group_id string) (*pb.Group, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}


	insertNew :=
		` update groups SET  admin = array_append(admin,$1) from users where users.id=$1 and  groups.id=$2 and $1!=all(groups.admin)

	`

	_, err = tx.Exec(
		insertNew,
		user_id,
		group_id,
	
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group_id)
	if err != nil {
		return nil, err
	}

	return c, nil

}
func (cm *groupRepo) RemoveMembers(user_id, group_id string) (*pb.Group, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update groups SET  users = array_remove(users,$1)  where   id=$2

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		group_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group_id)
	if err != nil {
		return nil, err
	}

	return c, nil

}
func (cm *groupRepo) RemoveAdmins(user_id, group_id string) (*pb.Group, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update groups SET  admin = array_remove(admin,$1)  where   id=$2

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		group_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetGroup(group_id)
	if err != nil {
		return nil, err
	}

	return c, nil
}
