package postgres

import (
	"database/sql"
	"fmt"
	pb "genproto/user_service"
	"time"

	// "github.com/google/uuid"

	"github.com/jmoiron/sqlx"

	"bitbucket.org/alien_soft/delever_user_service/storage/repo"
)

type chanelRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewChanelRepo(db *sqlx.DB) repo.ChanelStorageI {
	return &chanelRepo{
		db: db,
	}
}

func (cm *chanelRepo) Create(chanel *pb.Chanel) (*pb.Chanel, error) {
	tx, err := cm.db.Begin()

	if err != nil {
		return nil, err
	}
	insertNew :=
		`INSERT INTO
		chanels
		(
			id,
			access_token,
			name,
			user_name,
			bio,
			photo,
			owner,
			users,
			admin
		)
		VALUES
		($1, $2, $3, $4,$5,$6,$7,'{`+chanel.GetOwner()+`}','{`+chanel.GetOwner()+`}')`
	_, err = tx.Exec(
		insertNew,
		chanel.GetId(),
		chanel.GetAccessToken(),
		chanel.GetName(),
		chanel.GetUserName(),
		chanel.GetBio(),
		chanel.GetPhoto(),
		chanel.GetOwner(),
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel.GetId())
	if err != nil {
		return nil, err
	}
	fmt.Print(chanel.GetId())

	return c, nil

}
func (cm *chanelRepo) Update(chanel *pb.Chanel) (*pb.Chanel, error) {

	tx, err := cm.db.Begin()
	if err != nil {
		return nil, err
	}
	updateQuery :=
		`UPDATE chanels
		 SET
		    name=$1,
			user_name=$3,
			bio=$4,
			photo=$5
		WHERE id=$2`

	_, err = tx.Exec(
		updateQuery,
		chanel.GetName(),
		chanel.GetId(),
		chanel.GetUserName(),
		chanel.GetBio(),
		chanel.GetPhoto(),
	)
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel.GetId())
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cm *chanelRepo) Delete(id string) error {
	result, err := cm.db.Exec(`UPDATE chanels SET deleted_at=CURRENT_TIMESTAMP where id=$1 and deleted_at IS NULL`, id)
	if err != nil {
		return err
	}
	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (cm *chanelRepo) GetChanel(id string) (*pb.Chanel, error) {
	var (
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		chanel      pb.Chanel
	)

	row := cm.db.QueryRow(`
		SELECT  id,
				name,
				access_token,
				user_name,
				bio,
				photo, 
				owner,
				admin,
				users,
				created_at
		FROM chanels
		WHERE id=$1 and
		deleted_at IS NULL`, id,
	)

	err := row.Scan(
		&chanel.Id,
		&chanel.Name,
		&chanel.AccessToken,
		&chanel.UserName,
		&chanel.Bio,
		&chanel.Photo,
		&chanel.Owner,
		&chanel.Admin,
		&chanel.Users,
		&createdAt,
	)
	if err != nil {
		return nil, err
	}
	chanel.CreatedAt = createdAt.Format(layoutDate)
	if err != nil {
		return nil, err
	}
	return &chanel, nil
}

func (cm *chanelRepo) GetChanels(page, limit uint64) ([]*pb.Chanel, uint64, error) {
	var (
		count      uint64
		createdAt  time.Time
		layoutDate string = "2006-01-02 15:04:05"
		chanels     []*pb.Chanel
	)

	offset := (page - 1) * limit
	query := `
		SELECT  id,
				access_token,
				name,
				user_name,
				bio,
				photo,
				owner,
				users,
				admin,
				created_at
		FROM chanels
		WHERE deleted_at IS NULL 
		ORDER BY created_at DESC 
		LIMIT $1 OFFSET $2`
	rows, err := cm.db.Queryx(query, limit, offset)

	if err != nil {
		return nil, 0, err
	}
	for rows.Next() {
		var c pb.Chanel
		err = rows.Scan(
			&c.Id,
			&c.AccessToken,
			&c.Name,
			&c.UserName,
			&c.Bio,
			&c.Photo,
			&c.Owner,
			&c.Users,
			&c.Admin,
			&createdAt,
		)
		if err != nil {
			return nil, 0, err
		}
		c.CreatedAt = createdAt.Format(layoutDate)
		chanels = append(chanels, &c)
	}
	row := cm.db.QueryRow(`
		SELECT count(1) 
		FROM chanels
		WHERE deleted_at IS NULL`,
	)

	err = row.Scan(
		&count,
	)

	return chanels, count, nil
}

func (cm *chanelRepo) SearchChanelByName(name string, limit uint64) ([]*pb.Chanel, error) {
	var chanels []*pb.Chanel

	query := `
			SELECT  id,
					name,
					user_name,
					bio,
					photo,
					owner
			FROM chanels
			WHERE name LIKE '%' || $1 || '%' and 
			deleted_at IS NULL
			ORDER BY created_at DESC
			LIMIT $2 `
	rows, err := cm.db.Queryx(query, name, limit)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var c pb.Chanel
		err = rows.Scan(
			&c.Id,
			&c.Name,
			&c.UserName,
			&c.Bio,
			&c.Photo,
			&c.Owner,
		)

		if err != nil {
			return nil, err
		}
		chanels = append(chanels, &c)
	}
	return chanels, nil
}
func (cm *chanelRepo) AddMembers(user_id , chanel_id string) (*pb.Chanel, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update chanels SET  users = array_append(users,$1) from users  where users.id=$1  and chanels.id=$2 and $1!=all(chanels.users)

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		chanel_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel_id)
	if err != nil {
		return nil, err
	}

	return c, nil
}
func (cm *chanelRepo) AddAdmins(user_id, chanel_id string) (*pb.Chanel, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}


	insertNew :=
		` update chanels SET  admin = array_append(admin,$1) from users where users.id=$1  and chanels.id=$2 and $1!=all(chanels.admin)

	`

	_, err = tx.Exec(
		insertNew,
		user_id,
		chanel_id,
	
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel_id)
	if err != nil {
		return nil, err
	}

	return c, nil

}
func (cm *chanelRepo) RemoveMembers(user_id, chanel_id string) (*pb.Chanel, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update chanels SET  users = array_remove(users,$1)  where   id=$2

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		chanel_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel_id)
	if err != nil {
		return nil, err
	}

	return c, nil

}
func (cm *chanelRepo) RemoveAdmins(user_id, chanel_id string) (*pb.Chanel, error) {
	tx, err := cm.db.Begin()
	fmt.Println(user_id)
	if err != nil {
		return nil, err
	}
	insertNew :=
		` update chanels SET  admin = array_remove(admin,$1)  where   id=$2

	`
	_, err = tx.Exec(
		insertNew,
		user_id,
		chanel_id,
	)

	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()

	c, err := cm.GetChanel(chanel_id)
	if err != nil {
		return nil, err
	}

	return c, nil
}
