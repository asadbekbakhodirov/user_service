package storage

import (
	"github.com/jmoiron/sqlx"

	"bitbucket.org/alien_soft/delever_user_service/storage/postgres"
	"bitbucket.org/alien_soft/delever_user_service/storage/repo"
)

// StorageI ...
type StorageI interface {
	User()   repo.UserStorageI
	Group() repo.GroupStorageI
	Chanel() repo.ChanelStorageI

}

type storagePg struct {
	userRepo   repo.UserStorageI
	groupRepo repo.GroupStorageI
	chanelRepo repo.ChanelStorageI

}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		userRepo:   postgres.NewUserRepo(db),
		groupRepo: postgres.NewGroupRepo(db),
		chanelRepo: postgres.NewChanelRepo(db),

	}
}

func (s storagePg) User() repo.UserStorageI {
	return s.userRepo
}

func (s storagePg) Group() repo.GroupStorageI {
	return s.groupRepo
}
func (s storagePg) Chanel() repo.ChanelStorageI {
	return s.chanelRepo
}