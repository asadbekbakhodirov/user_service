package repo

import (
	pb "genproto/user_service"

	_ "github.com/lib/pq"
)

type UserStorageI interface {
	Create(client *pb.Client) (*pb.Client, error)
	Update(client *pb.Client) (*pb.Client, error)
	Delete(id string) error
	GetClient(id string) (*pb.Client, error)
	GetClients(page, limit uint64) ([]*pb.Client, uint64, error)
	SearchClientsByName(name string,limit uint64) ([]*pb.Client, error)

	

}
