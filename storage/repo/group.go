package repo

import (
	pb "genproto/user_service"

	_ "github.com/lib/pq"
)

type GroupStorageI interface {
	Create(group *pb.Group) (*pb.Group, error)
	Update(group *pb.Group) (*pb.Group, error)
	Delete(id string) error
	GetGroup(id string) (*pb.Group, error)
	GetGroups(page, limit uint64) ([]*pb.Group, uint64, error)
	SearchGroupByName(name string,limit uint64) ([]*pb.Group, error)
	AddMembers(user_id, group_id string)(*pb.Group,error)
	AddAdmins(user_id, group_id string)(*pb.Group,error)
	RemoveMembers(user_id, group_id string)(*pb.Group,error)
	RemoveAdmins(user_id, group_id string)(*pb.Group,error)




}