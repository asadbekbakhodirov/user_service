package repo

import (
	pb "genproto/user_service"

	_ "github.com/lib/pq"
)

type ChanelStorageI interface {
	Create(chanel *pb.Chanel) (*pb.Chanel, error)
	Update(chanel *pb.Chanel) (*pb.Chanel, error)
	Delete(id string) error
	GetChanel(id string) (*pb.Chanel, error)
	GetChanels(page, limit uint64) ([]*pb.Chanel, uint64, error)
	SearchChanelByName(name string,limit uint64) ([]*pb.Chanel, error)                           
	AddMembers(user_id, chanel_id string)(*pb.Chanel,error)
	AddAdmins(user_id, chanel_id string)(*pb.Chanel,error)
	RemoveMembers(user_id, chanel_id string)(*pb.Chanel,error)
	RemoveAdmins(user_id, chanel_id string)(*pb.Chanel,error)




}