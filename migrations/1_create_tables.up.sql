
CREATE TABLE IF NOT EXISTS users (
     id uuid PRIMARY KEY,
     access_token VARCHAR NOT NULL UNIQUE,
     name  VARCHAR(100) NOT NULL,
     phone VARCHAR(100) NOT NULL UNIQUE,
     user_name VARCHAR(100)  UNIQUE,
     bio VARCHAR(100),
     photo text,
     is_active BOOLEAN NOT NULL DEFAULT TRUE,
     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     updated_at TIMESTAMP,
     deleted_at TIMESTAMP
);



 
CREATE TABLE IF NOT EXISTS groups (
     id uuid PRIMARY KEY,
     access_token VARCHAR NOT NULL UNIQUE,
     name  VARCHAR(100) NOT NULL,
     user_name VARCHAR(100)  UNIQUE,
     bio VARCHAR(100),
     photo text,
     owner  VARCHAR(250) NOT NULL,
     admin  uuid[] ,
     users  uuid[] ,
     members integer,
     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      updated_at TIMESTAMP,
     deleted_at TIMESTAMP



);

CREATE TABLE IF NOT EXISTS chanels (
     id uuid PRIMARY KEY,
     access_token VARCHAR NOT NULL UNIQUE,
     name  VARCHAR(100) NOT NULL,
     user_name VARCHAR(100)  UNIQUE,
     bio VARCHAR(100),
     photo text,
     owner  VARCHAR(250) NOT NULL,
     admin  uuid[] ,
     users  uuid[] ,
     members integer,
     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      updated_at TIMESTAMP,
     deleted_at TIMESTAMP
);
