// Code generated by protoc-gen-go. DO NOT EDIT.
// source: chanel.proto

package user_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "github.com/golang/protobuf/ptypes/wrappers"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Chanel struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	AccessToken          string   `protobuf:"bytes,2,opt,name=access_token,json=accessToken,proto3" json:"access_token,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	UserName             string   `protobuf:"bytes,4,opt,name=user_name,json=userName,proto3" json:"user_name,omitempty"`
	Photo                string   `protobuf:"bytes,5,opt,name=photo,proto3" json:"photo,omitempty"`
	Bio                  string   `protobuf:"bytes,7,opt,name=bio,proto3" json:"bio,omitempty"`
	Owner                string   `protobuf:"bytes,8,opt,name=owner,proto3" json:"owner,omitempty"`
	Admin                string   `protobuf:"bytes,9,opt,name=admin,proto3" json:"admin,omitempty"`
	Users                string   `protobuf:"bytes,10,opt,name=users,proto3" json:"users,omitempty"`
	Members              uint64   `protobuf:"varint,11,opt,name=members,proto3" json:"members,omitempty"`
	CreatedAt            string   `protobuf:"bytes,12,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,13,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt            string   `protobuf:"bytes,14,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Chanel) Reset()         { *m = Chanel{} }
func (m *Chanel) String() string { return proto.CompactTextString(m) }
func (*Chanel) ProtoMessage()    {}
func (*Chanel) Descriptor() ([]byte, []int) {
	return fileDescriptor_cbc67a24b925856d, []int{0}
}

func (m *Chanel) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Chanel.Unmarshal(m, b)
}
func (m *Chanel) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Chanel.Marshal(b, m, deterministic)
}
func (m *Chanel) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Chanel.Merge(m, src)
}
func (m *Chanel) XXX_Size() int {
	return xxx_messageInfo_Chanel.Size(m)
}
func (m *Chanel) XXX_DiscardUnknown() {
	xxx_messageInfo_Chanel.DiscardUnknown(m)
}

var xxx_messageInfo_Chanel proto.InternalMessageInfo

func (m *Chanel) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Chanel) GetAccessToken() string {
	if m != nil {
		return m.AccessToken
	}
	return ""
}

func (m *Chanel) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Chanel) GetUserName() string {
	if m != nil {
		return m.UserName
	}
	return ""
}

func (m *Chanel) GetPhoto() string {
	if m != nil {
		return m.Photo
	}
	return ""
}

func (m *Chanel) GetBio() string {
	if m != nil {
		return m.Bio
	}
	return ""
}

func (m *Chanel) GetOwner() string {
	if m != nil {
		return m.Owner
	}
	return ""
}

func (m *Chanel) GetAdmin() string {
	if m != nil {
		return m.Admin
	}
	return ""
}

func (m *Chanel) GetUsers() string {
	if m != nil {
		return m.Users
	}
	return ""
}

func (m *Chanel) GetMembers() uint64 {
	if m != nil {
		return m.Members
	}
	return 0
}

func (m *Chanel) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Chanel) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

func (m *Chanel) GetDeletedAt() string {
	if m != nil {
		return m.DeletedAt
	}
	return ""
}

func init() {
	proto.RegisterType((*Chanel)(nil), "genproto.Chanel")
}

func init() { proto.RegisterFile("chanel.proto", fileDescriptor_cbc67a24b925856d) }

var fileDescriptor_cbc67a24b925856d = []byte{
	// 272 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x34, 0x90, 0xb1, 0x4e, 0xc3, 0x30,
	0x10, 0x40, 0x95, 0x34, 0x6d, 0x93, 0x6b, 0xa8, 0x90, 0x05, 0xc2, 0x02, 0x81, 0x0a, 0x53, 0x27,
	0x32, 0xf0, 0x05, 0x85, 0x9d, 0xa1, 0x62, 0x62, 0x89, 0x9c, 0xf8, 0x48, 0x23, 0x12, 0x3b, 0xb2,
	0x1d, 0xfa, 0x95, 0xfc, 0x53, 0xe5, 0x73, 0xb2, 0xdd, 0x7b, 0xef, 0xec, 0xe1, 0x20, 0xaf, 0x4f,
	0x42, 0x61, 0xf7, 0x3a, 0x18, 0xed, 0x34, 0x4b, 0x1b, 0x54, 0x34, 0xdd, 0x3f, 0x35, 0x5a, 0x37,
	0x1d, 0x16, 0x44, 0xd5, 0xf8, 0x53, 0x9c, 0x8d, 0x18, 0x06, 0x34, 0x36, 0x6c, 0xbe, 0xfc, 0xc7,
	0xb0, 0xfa, 0xa0, 0xa7, 0x6c, 0x0b, 0x71, 0x2b, 0x79, 0xb4, 0x8b, 0xf6, 0xd9, 0x31, 0x6e, 0x25,
	0x7b, 0x86, 0x5c, 0xd4, 0x35, 0x5a, 0x5b, 0x3a, 0xfd, 0x8b, 0x8a, 0xc7, 0x54, 0x36, 0xc1, 0x7d,
	0x79, 0xc5, 0x18, 0x24, 0x4a, 0xf4, 0xc8, 0x17, 0x94, 0x68, 0x66, 0x0f, 0x90, 0x8d, 0x16, 0x4d,
	0x49, 0x21, 0xa1, 0x90, 0x7a, 0xf1, 0xe9, 0xe3, 0x0d, 0x2c, 0x87, 0x93, 0x76, 0x9a, 0x2f, 0x29,
	0x04, 0x60, 0xd7, 0xb0, 0xa8, 0x5a, 0xcd, 0xd7, 0xe4, 0xfc, 0xe8, 0xf7, 0xf4, 0x59, 0xa1, 0xe1,
	0x69, 0xd8, 0x23, 0xf0, 0x56, 0xc8, 0xbe, 0x55, 0x3c, 0x0b, 0x96, 0xc0, 0x5b, 0xff, 0xbf, 0xe5,
	0x10, 0x2c, 0x01, 0xe3, 0xb0, 0xee, 0xb1, 0xaf, 0xbc, 0xdf, 0xec, 0xa2, 0x7d, 0x72, 0x9c, 0x91,
	0x3d, 0x02, 0xd4, 0x06, 0x85, 0x43, 0x59, 0x0a, 0xc7, 0x73, 0x7a, 0x94, 0x4d, 0xe6, 0xe0, 0x7c,
	0x1e, 0x07, 0x39, 0xe7, 0xab, 0x90, 0x27, 0x13, 0xb2, 0xc4, 0x0e, 0xa7, 0xbc, 0x0d, 0x79, 0x32,
	0x07, 0xf7, 0x7e, 0xf7, 0x7d, 0x3b, 0xdf, 0xbe, 0xa0, 0x33, 0x58, 0x34, 0x7f, 0x6d, 0x8d, 0xd5,
	0x8a, 0xdc, 0xdb, 0x25, 0x00, 0x00, 0xff, 0xff, 0x44, 0x18, 0x7e, 0xfd, 0xa9, 0x01, 0x00, 0x00,
}
