// Code generated by protoc-gen-go. DO NOT EDIT.
// source: co.proto

package co_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CargoOwner struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Logo                 string   `protobuf:"bytes,3,opt,name=logo,proto3" json:"logo,omitempty"`
	Description          string   `protobuf:"bytes,4,opt,name=description,proto3" json:"description,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,5,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	IsActive             bool     `protobuf:"varint,6,opt,name=is_active,json=isActive,proto3" json:"is_active,omitempty"`
	Token                string   `protobuf:"bytes,7,opt,name=token,proto3" json:"token,omitempty"`
	Login                string   `protobuf:"bytes,8,opt,name=login,proto3" json:"login,omitempty"`
	Password             string   `protobuf:"bytes,9,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CargoOwner) Reset()         { *m = CargoOwner{} }
func (m *CargoOwner) String() string { return proto.CompactTextString(m) }
func (*CargoOwner) ProtoMessage()    {}
func (*CargoOwner) Descriptor() ([]byte, []int) {
	return fileDescriptor_7e75b5f62bea86ab, []int{0}
}

func (m *CargoOwner) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CargoOwner.Unmarshal(m, b)
}
func (m *CargoOwner) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CargoOwner.Marshal(b, m, deterministic)
}
func (m *CargoOwner) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CargoOwner.Merge(m, src)
}
func (m *CargoOwner) XXX_Size() int {
	return xxx_messageInfo_CargoOwner.Size(m)
}
func (m *CargoOwner) XXX_DiscardUnknown() {
	xxx_messageInfo_CargoOwner.DiscardUnknown(m)
}

var xxx_messageInfo_CargoOwner proto.InternalMessageInfo

func (m *CargoOwner) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *CargoOwner) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CargoOwner) GetLogo() string {
	if m != nil {
		return m.Logo
	}
	return ""
}

func (m *CargoOwner) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *CargoOwner) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *CargoOwner) GetIsActive() bool {
	if m != nil {
		return m.IsActive
	}
	return false
}

func (m *CargoOwner) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

func (m *CargoOwner) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *CargoOwner) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type Location struct {
	Long                 float32  `protobuf:"fixed32,1,opt,name=long,proto3" json:"long,omitempty"`
	Lat                  float32  `protobuf:"fixed32,2,opt,name=lat,proto3" json:"lat,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Location) Reset()         { *m = Location{} }
func (m *Location) String() string { return proto.CompactTextString(m) }
func (*Location) ProtoMessage()    {}
func (*Location) Descriptor() ([]byte, []int) {
	return fileDescriptor_7e75b5f62bea86ab, []int{1}
}

func (m *Location) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Location.Unmarshal(m, b)
}
func (m *Location) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Location.Marshal(b, m, deterministic)
}
func (m *Location) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Location.Merge(m, src)
}
func (m *Location) XXX_Size() int {
	return xxx_messageInfo_Location.Size(m)
}
func (m *Location) XXX_DiscardUnknown() {
	xxx_messageInfo_Location.DiscardUnknown(m)
}

var xxx_messageInfo_Location proto.InternalMessageInfo

func (m *Location) GetLong() float32 {
	if m != nil {
		return m.Long
	}
	return 0
}

func (m *Location) GetLat() float32 {
	if m != nil {
		return m.Lat
	}
	return 0
}

type CargoOwnerBranch struct {
	Id                   string    `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string    `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	CoId                 string    `protobuf:"bytes,3,opt,name=co_id,json=coId,proto3" json:"co_id,omitempty"`
	Location             *Location `protobuf:"bytes,4,opt,name=location,proto3" json:"location,omitempty"`
	Address              string    `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
	DestinationAddress   string    `protobuf:"bytes,6,opt,name=destination_address,json=destinationAddress,proto3" json:"destination_address,omitempty"`
	Description          string    `protobuf:"bytes,7,opt,name=description,proto3" json:"description,omitempty"`
	IsActive             bool      `protobuf:"varint,8,opt,name=is_active,json=isActive,proto3" json:"is_active,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *CargoOwnerBranch) Reset()         { *m = CargoOwnerBranch{} }
func (m *CargoOwnerBranch) String() string { return proto.CompactTextString(m) }
func (*CargoOwnerBranch) ProtoMessage()    {}
func (*CargoOwnerBranch) Descriptor() ([]byte, []int) {
	return fileDescriptor_7e75b5f62bea86ab, []int{2}
}

func (m *CargoOwnerBranch) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CargoOwnerBranch.Unmarshal(m, b)
}
func (m *CargoOwnerBranch) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CargoOwnerBranch.Marshal(b, m, deterministic)
}
func (m *CargoOwnerBranch) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CargoOwnerBranch.Merge(m, src)
}
func (m *CargoOwnerBranch) XXX_Size() int {
	return xxx_messageInfo_CargoOwnerBranch.Size(m)
}
func (m *CargoOwnerBranch) XXX_DiscardUnknown() {
	xxx_messageInfo_CargoOwnerBranch.DiscardUnknown(m)
}

var xxx_messageInfo_CargoOwnerBranch proto.InternalMessageInfo

func (m *CargoOwnerBranch) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *CargoOwnerBranch) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *CargoOwnerBranch) GetCoId() string {
	if m != nil {
		return m.CoId
	}
	return ""
}

func (m *CargoOwnerBranch) GetLocation() *Location {
	if m != nil {
		return m.Location
	}
	return nil
}

func (m *CargoOwnerBranch) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *CargoOwnerBranch) GetDestinationAddress() string {
	if m != nil {
		return m.DestinationAddress
	}
	return ""
}

func (m *CargoOwnerBranch) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

func (m *CargoOwnerBranch) GetIsActive() bool {
	if m != nil {
		return m.IsActive
	}
	return false
}

func init() {
	proto.RegisterType((*CargoOwner)(nil), "genproto.CargoOwner")
	proto.RegisterType((*Location)(nil), "genproto.Location")
	proto.RegisterType((*CargoOwnerBranch)(nil), "genproto.CargoOwnerBranch")
}

func init() { proto.RegisterFile("co.proto", fileDescriptor_7e75b5f62bea86ab) }

var fileDescriptor_7e75b5f62bea86ab = []byte{
	// 339 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x51, 0xcd, 0x4e, 0xf3, 0x30,
	0x10, 0x54, 0xd2, 0x3f, 0x77, 0xfb, 0xe9, 0x53, 0xe5, 0x82, 0x64, 0xc1, 0xa5, 0xf4, 0xd4, 0x53,
	0x8a, 0xe0, 0x09, 0x5a, 0x4e, 0x48, 0x08, 0xa4, 0x1c, 0xb9, 0x44, 0xae, 0x6d, 0xa5, 0x16, 0xa9,
	0x37, 0xb2, 0x43, 0xfb, 0x04, 0xbc, 0x2e, 0xcf, 0x80, 0xb2, 0x69, 0x4a, 0x0b, 0x17, 0x6e, 0x33,
	0xb3, 0x63, 0x79, 0x77, 0x06, 0x98, 0xc2, 0xa4, 0xf4, 0x58, 0x21, 0x67, 0xb9, 0x71, 0x84, 0x66,
	0x9f, 0x11, 0xc0, 0x83, 0xf4, 0x39, 0xbe, 0xec, 0x9d, 0xf1, 0xfc, 0x3f, 0xc4, 0x56, 0x8b, 0x68,
	0x1a, 0xcd, 0x87, 0x69, 0x6c, 0x35, 0xe7, 0xd0, 0x75, 0x72, 0x6b, 0x44, 0x4c, 0x0a, 0xe1, 0x5a,
	0x2b, 0x30, 0x47, 0xd1, 0x69, 0xb4, 0x1a, 0xf3, 0x29, 0x8c, 0xb4, 0x09, 0xca, 0xdb, 0xb2, 0xb2,
	0xe8, 0x44, 0x97, 0x46, 0xa7, 0x12, 0xbf, 0x81, 0x7f, 0xe5, 0x06, 0x9d, 0xc9, 0xdc, 0xfb, 0x76,
	0x6d, 0xbc, 0xe8, 0x35, 0x16, 0xd2, 0x9e, 0x49, 0xe2, 0xd7, 0x30, 0xb4, 0x21, 0x93, 0xaa, 0xb2,
	0x3b, 0x23, 0xfa, 0xd3, 0x68, 0xce, 0x52, 0x66, 0xc3, 0x92, 0x38, 0xbf, 0x80, 0x5e, 0x85, 0x6f,
	0xc6, 0x89, 0x01, 0x3d, 0x6c, 0x48, 0xad, 0x16, 0x98, 0x5b, 0x27, 0x58, 0xa3, 0x12, 0xe1, 0x57,
	0xc0, 0x4a, 0x19, 0xc2, 0x1e, 0xbd, 0x16, 0x43, 0x1a, 0x1c, 0xf9, 0xec, 0x16, 0xd8, 0x13, 0x2a,
	0x49, 0x3b, 0xd1, 0x25, 0x2e, 0xa7, 0x7b, 0xe3, 0x94, 0x30, 0x1f, 0x43, 0xa7, 0x90, 0x15, 0x1d,
	0x1c, 0xa7, 0x35, 0x9c, 0x7d, 0xc4, 0x30, 0xfe, 0x8e, 0x68, 0xe5, 0xa5, 0x53, 0x9b, 0x3f, 0x05,
	0x35, 0x81, 0x9e, 0xc2, 0xcc, 0xea, 0x36, 0x29, 0x85, 0x8f, 0x9a, 0x27, 0xc0, 0x8a, 0xc3, 0xff,
	0x14, 0xd3, 0xe8, 0x8e, 0x27, 0x6d, 0x1b, 0x49, 0xbb, 0x59, 0x7a, 0xf4, 0x70, 0x01, 0x03, 0xa9,
	0xb5, 0x37, 0x21, 0x1c, 0x22, 0x6b, 0x29, 0x5f, 0xc0, 0x44, 0x9b, 0x50, 0x59, 0x47, 0xc6, 0xac,
	0x75, 0xf5, 0xc9, 0xc5, 0x4f, 0x46, 0xcb, 0xc3, 0x83, 0x1f, 0x25, 0x0d, 0x7e, 0x97, 0x74, 0xd6,
	0x00, 0x3b, 0x6f, 0x60, 0x75, 0xf9, 0x3a, 0x69, 0x17, 0x5d, 0x28, 0xcc, 0x82, 0xf1, 0x3b, 0xab,
	0xcc, 0xba, 0x4f, 0xca, 0xfd, 0x57, 0x00, 0x00, 0x00, 0xff, 0xff, 0x0c, 0x6a, 0x07, 0xe7, 0x5e,
	0x02, 0x00, 0x00,
}
