package service

import (
	"context"
	"database/sql"
	pb "genproto/user_service"

	gpb "github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	l "bitbucket.org/alien_soft/delever_user_service/pkg/logger"
	"bitbucket.org/alien_soft/delever_user_service/storage"
)

// GroupService ...
type GroupService struct {
	storage storage.StorageI
	logger  l.Logger
}

// NewGroupService ...
func NewGroupService(db *sqlx.DB, log l.Logger) *GroupService {
	return &GroupService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

// CreateGroup is function for creating a courier
func (s *GroupService) CreateGroup(ctx context.Context, req *pb.CreateGroupRequest) (*pb.CreateGroupResponse, error) {
	Group, err := s.storage.Group().Create(req.Group)
	if err != nil {
		s.logger.Error("Error while creating Group in service", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.CreateGroupResponse{
		Group: Group,
	}, nil
}

// UpdateGroup is function for updating a Group
func (s *GroupService) UpdateGroup(ctx context.Context, req *pb.UpdateGroupRequest) (*pb.UpdateGroupResponse, error) {
	Group, err := s.storage.Group().Update(req.Group)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Group, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.UpdateGroupResponse{
		Group: Group,
	}, nil
}

//DeleteGroup if function for deleting Group
func (s *GroupService) DeleteGroup(ctx context.Context, req *pb.DeleteGroupRequest) (*gpb.Empty, error) {
	err := s.storage.Group().Delete(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while deleting Group, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while deleting Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &gpb.Empty{}, nil
}

// GetGroup is function for getting a Group
func (s *GroupService) GetGroup(ctx context.Context, req *pb.GetGroupRequest) (*pb.GetGroupResponse, error) {
	Group, err := s.storage.Group().GetGroup(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting an Group, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &pb.GetGroupResponse{
		Group: Group,
	}, nil
}

// GetAllGroups is function for getting all couriers
func (s *GroupService) GetAllGroups(ctx context.Context, req *pb.GetAllGroupsRequest) (*pb.GetAllGroupsResponse, error) {
	var Groups []*pb.Group

	Groups, count, err := s.storage.Group().GetGroups(req.GetPage(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all Groups, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Groups", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllGroupsResponse{
		Groups: Groups,
		Count:  count,
	}, nil
}

// SearchClientsByName ....
func (s *GroupService) SearchGroupByName(ctx context.Context, req *pb.SearchGroupByNameRequest) (*pb.SearchGroupByNameResponse, error) {
	var client []*pb.Group

	client, err := s.storage.Group().SearchGroupByName(req.GetName(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all Name list in search by Name, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Name list in search by Name", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.SearchGroupByNameResponse{
		Groups: client,
	}, nil
}
func (s *GroupService) AddMembers(ctx context.Context, req *pb.AddMemberRequest) (*pb.AddMemberResponse, error) {

	Group, err := s.storage.Group().AddMembers(req.GetUserId(), req.GetGroupId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Group, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.AddMemberResponse{
		Group: Group,
	}, nil
}
func (s *GroupService) AddAdmins(ctx context.Context, req *pb.AddAdminRequest) (*pb.AddAdminResponse, error) {
	// fmt.Println(req.GetGroupId())
	Group, err := s.storage.Group().AddAdmins(req.GetUserId(), req.GetGroupId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Group, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.AddAdminResponse{
		Group: Group,
	}, nil
}
func (s *GroupService) RemoveMembers(ctx context.Context, req *pb.RemoveMemberRequest) (*pb.RemoveMemberResponse, error) {

	Group, err := s.storage.Group().RemoveMembers(req.GetUserId(), req.GetGroupId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Group, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.RemoveMemberResponse{
		Group: Group,
	}, nil
}
func (s *GroupService) RemoveAdmins(ctx context.Context, req *pb.RemoveAdminRequest) (*pb.RemoveAdminResponse, error) {

	Group, err := s.storage.Group().RemoveAdmins(req.GetUserId(), req.GetGroupId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Group, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Group", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.RemoveAdminResponse{
		Group: Group,
	}, nil
}
