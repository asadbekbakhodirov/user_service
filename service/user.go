package service

import (
	"context"
	"database/sql"
	"fmt"
	pb "genproto/user_service"

	gpb "github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	l "bitbucket.org/alien_soft/delever_user_service/pkg/logger"
	"bitbucket.org/alien_soft/delever_user_service/storage"
)

// UserService ...
type UserService struct {
	storage storage.StorageI
	logger  l.Logger
}

// NewUserService ...
func NewUserService(strg storage.StorageI, log l.Logger) *UserService {
	return &UserService{
		storage: strg,
		logger:  log,
	}
}

// CreateClient is function for creating a courier
func (s *UserService) CreateClient(ctx context.Context, req *pb.Client) (*pb.CreateClientResponse, error) {
	client, err := s.storage.User().Create(req)
	if err != nil {
		s.logger.Error("Error while creating user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.CreateClientResponse{
		Client: client,
	}, nil
}

// UpdateClient is function for updating a user
func (s *UserService) UpdateClient(ctx context.Context, req *pb.Client) (*pb.UpdateClientResponse, error) {
	client, err := s.storage.User().Update(req)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.UpdateClientResponse{
		Client: client,
	}, nil
}

//DeleteClient if function for deleting user
func (s *UserService) DeleteClient(ctx context.Context, req *pb.DeleteClientRequest) (*gpb.Empty, error) {
	fmt.Print(req.GetId())
	err := s.storage.User().Delete(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while deleting user, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while deleting user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &gpb.Empty{}, nil
}

// GetClient is function for getting a user
func (s *UserService) GetClient(ctx context.Context, req *pb.GetClientRequest) (*pb.GetClientResponse, error) {
	client, err := s.storage.User().GetClient(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting an user, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &pb.GetClientResponse{
		Client: client,
	}, nil
}

// GetAllClients is function for getting all couriers
func (s *UserService) GetAllClients(ctx context.Context, req *pb.GetAllClientsRequest) (*pb.GetAllResponse, error) {
	var clients []*pb.Client

	clients, count, err := s.storage.User().GetClients(req.GetPage(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all users, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all users", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllResponse{
		Clients: clients,
		Count:   count,
	}, nil
}

// SearchClientsByName ....
func (s *UserService) SearchClientsByName(ctx context.Context, req *pb.SearchClientsByNameRequest) (*pb.SearchClientsByNameResponse, error) {
	var client []*pb.Client

	client, err := s.storage.User().SearchClientsByName(req.GetName(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all Name list in search by Name, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Name list in search by Name", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.SearchClientsByNameResponse{
		Clients: client,                    
	}, nil
}
