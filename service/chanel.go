package service

import (
	"context"
	"database/sql"
	pb "genproto/user_service"

	gpb "github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	l "bitbucket.org/alien_soft/delever_user_service/pkg/logger"
	"bitbucket.org/alien_soft/delever_user_service/storage"
)

// GroupService ...
type ChanelService struct {
	storage storage.StorageI
	logger  l.Logger
}

// NewChanelService ...
func NewChanelService(db *sqlx.DB, log l.Logger) *ChanelService {
	return &ChanelService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

// CreateChanel is function for creating a courier
func (s *ChanelService) CreateChanel(ctx context.Context, req *pb.CreateChanelRequest) (*pb.CreateChanelResponse, error) {
	Chanel, err := s.storage.Chanel().Create(req.Chanel)
	if err != nil {
		s.logger.Error("Error while creating Chanel in service", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.CreateChanelResponse{
		Chanel: Chanel,
	}, nil
}

// UpdateChanel is function for updating a Chanel
func (s *ChanelService) UpdateChanel(ctx context.Context, req *pb.UpdateChanelRequest) (*pb.UpdateChanelResponse, error) {
	Chanel, err := s.storage.Chanel().Update(req.Chanel)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Chanel, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.UpdateChanelResponse{
		Chanel: Chanel,
	}, nil
}

//DeleteChanel if function for deleting Chanel
func (s *ChanelService) DeleteChanel(ctx context.Context, req *pb.DeleteChanelRequest) (*gpb.Empty, error) {
	err := s.storage.Chanel().Delete(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while deleting Chanel, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while deleting Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &gpb.Empty{}, nil
}

// GetChanel is function for getting a Chanel
func (s *ChanelService) GetChanel(ctx context.Context, req *pb.GetChanelRequest) (*pb.GetChanelResponse, error) {
	Chanel, err := s.storage.Chanel().GetChanel(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting an Chanel, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	return &pb.GetChanelResponse{
		Chanel: Chanel,
	}, nil
}

// GetAllChanels is function for getting all couriers
func (s *ChanelService) GetAllChanels(ctx context.Context, req *pb.GetAllChanelsRequest) (*pb.GetAllChanelsResponse, error) {
	var Chanels []*pb.Chanel

	Chanels, count, err := s.storage.Chanel().GetChanels(req.GetPage(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all Chanels, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Chanels", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllChanelsResponse{
		Chanels: Chanels,
		Count:  count,
	}, nil
}

// SearchClientsByName ....
func (s *ChanelService) SearchChanelByName(ctx context.Context, req *pb.SearchChanelByNameRequest) (*pb.SearchChanelByNameResponse, error) {
	var client []*pb.Chanel

	client, err := s.storage.Chanel().SearchChanelByName(req.GetName(), req.GetLimit())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting all Name list in search by Name, Not found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting all Name list in search by Name", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.SearchChanelByNameResponse{
		Chanels: client,
	}, nil
}
func (s *ChanelService) AddMembers(ctx context.Context, req *pb.AddMemberChanelRequest) (*pb.AddMemberChanelResponse, error) {

	Chanel, err := s.storage.Chanel().AddMembers(req.GetUserId(), req.GetChanelId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Chanel, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.AddMemberChanelResponse{
		Chanel: Chanel,
	}, nil
}
func (s *ChanelService) AddAdmins(ctx context.Context, req *pb.AddAdminChanelRequest) (*pb.AddAdminChanelResponse, error) {
	// fmt.Println(req.GetChanelId())
	Chanel, err := s.storage.Chanel().AddAdmins(req.GetUserId(), req.GetChanelId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Chanel, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.AddAdminChanelResponse{
		Chanel: Chanel,
	}, nil
}
func (s *ChanelService) RemoveMembers(ctx context.Context, req *pb.RemoveMemberChanelRequest) (*pb.RemoveMemberChanelResponse, error) {

	Chanel, err := s.storage.Chanel().RemoveMembers(req.GetUserId(), req.GetChanelId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Chanel, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.RemoveMemberChanelResponse{
		Chanel: Chanel,
	}, nil
}
func (s *ChanelService) RemoveAdmins(ctx context.Context, req *pb.RemoveAdminChanelRequest) (*pb.RemoveAdminChanelResponse, error) {

	Chanel, err := s.storage.Chanel().RemoveAdmins(req.GetUserId(), req.GetChanelId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating Chanel, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating Chanel", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.RemoveAdminChanelResponse{
		Chanel: Chanel,
	}, nil
}
