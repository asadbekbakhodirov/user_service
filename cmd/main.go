package main

import (
	"fmt"
	pb "genproto/user_service"
	"net"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"

	"bitbucket.org/alien_soft/delever_user_service/config"
	"bitbucket.org/alien_soft/delever_user_service/pkg/logger"
	"bitbucket.org/alien_soft/delever_user_service/service"
	"bitbucket.org/alien_soft/delever_user_service/storage"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "user_service")
	defer logger.Cleanup(log)

	log.Info("main: pgxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase)

	connDb, err := sqlx.Connect("postgres", psqlString)
	if err != nil {
		log.Error("Error while connecting database: %v", logger.Error(err))
		return
	}

	storage := storage.NewStoragePg(connDb)

	userService := service.NewUserService(storage, log)
	groupService := service.NewGroupService(connDb,log)
	chanelService := service.NewChanelService(connDb,log)



	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	s := grpc.NewServer()

	pb.RegisterUserServiceServer(s, userService)
	pb.RegisterGroupServiceServer(s, groupService)
	pb.RegisterChanelServiceServer(s, chanelService)



	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
