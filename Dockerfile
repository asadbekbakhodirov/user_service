# workspace (GOPATH) configured at /go
FROM golang:1.13.1 as builder


#
RUN mkdir -p $GOPATH/src/bitbucket.org/alien_soft/delever_user_service
WORKDIR $GOPATH/src/bitbucket.org/alien_soft/delever_user_service

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/delever_user_service /



FROM alpine
COPY --from=builder delever_user_service .
ENTRYPOINT ["/delever_user_service"]



